
#################################################################
# Summary Statistics
#################################################################

DIC = 114.2814

                                     Mean    SD  2.5%    5%   25%   50%
Epsilon.1                           4.645 2.051 1.798 2.131 3.180 4.237
Epsilon.2                           0.294 0.080 0.170 0.187 0.238 0.282
site.SD                             8.896 5.710 0.384 0.959 4.007 8.070
p.Chiendent.MicroPhytoBenthos       0.452 0.264 0.031 0.061 0.225 0.437
p.Obione.MicroPhytoBenthos          0.303 0.271 0.007 0.012 0.074 0.215
p.Puccinellie.MicroPhytoBenthos     0.432 0.302 0.014 0.028 0.156 0.395
p.Chiendent.POM                     0.258 0.222 0.006 0.011 0.071 0.196
p.Obione.POM                        0.259 0.264 0.003 0.005 0.044 0.152
p.Puccinellie.POM                   0.277 0.271 0.002 0.005 0.046 0.178
p.Chiendent.Terr_veg                0.290 0.226 0.011 0.020 0.101 0.233
p.Obione.Terr_veg                   0.439 0.301 0.010 0.021 0.165 0.407
p.Puccinellie.Terr_veg              0.291 0.266 0.004 0.009 0.064 0.202
p.Chiendent.BDS.MicroPhytoBenthos   0.957 0.067 0.770 0.810 0.937 0.993
p.Chiendent.BDS.POM                 0.013 0.031 0.000 0.000 0.000 0.000
p.Chiendent.BDS.Terr_veg            0.029 0.058 0.000 0.000 0.000 0.000
p.Chiendent.BDM.MicroPhytoBenthos   0.949 0.048 0.830 0.855 0.921 0.959
p.Chiendent.BDM.POM                 0.007 0.015 0.000 0.000 0.000 0.000
p.Chiendent.BDM.Terr_veg            0.045 0.045 0.000 0.000 0.005 0.034
p.Obione.BDM.MicroPhytoBenthos      0.839 0.114 0.649 0.669 0.752 0.820
p.Obione.BDM.POM                    0.012 0.028 0.000 0.000 0.000 0.000
p.Obione.BDM.Terr_veg               0.148 0.111 0.000 0.000 0.014 0.165
p.Puccinellie.BDS.MicroPhytoBenthos 0.938 0.105 0.630 0.707 0.917 0.994
p.Puccinellie.BDS.POM               0.026 0.063 0.000 0.000 0.000 0.000
p.Puccinellie.BDS.Terr_veg          0.036 0.085 0.000 0.000 0.000 0.000
                                       75%    95%  97.5%
Epsilon.1                            5.652  8.619  9.798
Epsilon.2                            0.337  0.441  0.479
site.SD                             13.659 18.764 19.524
p.Chiendent.MicroPhytoBenthos        0.666  0.894  0.924
p.Obione.MicroPhytoBenthos           0.489  0.851  0.912
p.Puccinellie.MicroPhytoBenthos      0.690  0.934  0.957
p.Chiendent.POM                      0.399  0.705  0.778
p.Obione.POM                         0.421  0.824  0.885
p.Puccinellie.POM                    0.454  0.822  0.889
p.Chiendent.Terr_veg                 0.434  0.728  0.796
p.Obione.Terr_veg                    0.707  0.927  0.957
p.Puccinellie.Terr_veg               0.471  0.828  0.898
p.Chiendent.BDS.MicroPhytoBenthos    1.000  1.000  1.000
p.Chiendent.BDS.POM                  0.011  0.075  0.106
p.Chiendent.BDS.Terr_veg             0.027  0.168  0.210
p.Chiendent.BDM.MicroPhytoBenthos    0.990  1.000  1.000
p.Chiendent.BDM.POM                  0.005  0.036  0.052
p.Chiendent.BDM.Terr_veg             0.070  0.134  0.157
p.Obione.BDM.MicroPhytoBenthos       0.962  1.000  1.000
p.Obione.BDM.POM                     0.009  0.075  0.099
p.Obione.BDM.Terr_veg                0.236  0.317  0.340
p.Puccinellie.BDS.MicroPhytoBenthos  1.000  1.000  1.000
p.Puccinellie.BDS.POM                0.011  0.171  0.246
p.Puccinellie.BDS.Terr_veg           0.024  0.216  0.335
